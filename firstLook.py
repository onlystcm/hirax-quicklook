# DASH dependencies
import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate

# StratoDem Analytics implementations of material-ui components for Dash
import sd_material_ui as sdmu

# Plotly dependencies
import plotly.tools as tls
import plotly.graph_objs as go

# HDF5 dependencies for binary data format
import h5py

# Import moduule for HiRAX Tools
from hirax_tools.core import RawData

# Numpy dependencies
import numpy as np

# Import classes
import graphClasses as gc

# Import debugger
import pdb

# Used for memory calculation
from sys import getsizeof

# Import time modules
from datetime import datetime, timedelta, timezone
from dateutil.parser import parse as dateparse
import time

# Pandas
import pandas as pd

# pickle library for serialization
import pickle

# Pickle library to host global variables
import pickle

# Redis Server connection
import redis

# Sharing data between callbacks, on Apache Plasma. Built for Dash, useful anywhere.
from brain_plasma import Brain

# Get a brain
#brain = Brain(size=2000000000)

#print(brain.mb)

# Create a redis client
redisClient = redis.StrictRedis(host='localhost', port=6379, db=0, charset="utf-8")


def dictionaries(vis, lower=None, upper=None):
    # Get list of frequencies in data
    freq_dict = [{'label': str('{:.1f} GHz'.format(frequency)), 'value': frequency} for frequency in vis['freq']]
    # Get list of times in data
    time_dict = [{'label': datetime.fromtimestamp(float(time)).strftime("%d/%m/%Y, %H:%M:%S"), 'value': float(time)} for time in vis['times'][lower:upper]]
    return freq_dict, time_dict

fileframes = pd.read_pickle("../output_index.df")

#filename_list = ['../Data Files/05992409_0000.h5', '../Data Files/06003404_0000.h5', '../Data Files/06014399_0000.h5', '../Data Files/06025394_0000.h5', '../Data Files/06036389_0000.h5', '../Data Files/06047384_0000.h5']

with open("../frequency_stamps.txt", "r") as ins:
    frequencies = []
    for line in ins:
        frequencies.append(float(line.strip()))
frequencies = np.array(frequencies)

# Array of visibility objects per client
visGraph = [[] for x in range(100)]

#brain['memorybank'] = visGraph
#brain['clients'] = 0

# Set a string value for the key named 'Language'
#redisClient.set('visGraph')
redisClient.set('clients', 0)
#clients = 0

frequency_dictionary = [{'label': str('{:.1f} GHz'.format(frequency)), 'value': frequency} for frequency in frequencies]

unixdatetimes = []
for date in fileframes.index:
    unixdatetimes.append(date.timestamp())

timestamp_dictionary = [{'label': datetime.fromtimestamp(timestamp).strftime("%d/%m/%Y, %H:%M:%S"), 'value': timestamp} for timestamp in unixdatetimes]

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP, 'https://codepen.io/chriddyp/pen/bWLwgP.css'])   # Start new instance
app.css.append_css({'external_url': 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'})  # Additional fonts


colors = {  
    # Set predefined colours
    'background': '#FFFFFF',
    'text': '#000000'
}

server = app.server

app.layout = html.Div(style={'backgroundColor': colors['background']}, children=[

    # Save memory used for callbacks and refreshes
    dcc.Store(id='local-storage', storage_type='local'),

    sdmu.Dialog(id='data-selector-dialog', open=True, modal=True, children=
        [
            dbc.Row(children=[
                dbc.Col(children=
                    html.H3(children='Select the required time frame for analysis', style={'textAlign': 'center', 'color': colors['text']}),
                    width={"size": 12},
                    align='center'
                    ),
                ],
            ),

            html.Br(),

            dbc.Row(children=[
                dbc.Col(children=
                    dbc.Checklist(id="calander-checkbox",
                        options=[ {"label": "Use Calendar", "value": True},],
                            values=[],
                            inline=True,
                        ),
                    width={"size": 3},
                    align='center'
                ),
                dbc.Col(children=[
                    html.Div(id="time-value-container", children= [
                        dbc.InputGroup(children=[
                            dcc.Input(id='time-value', value=1, type="number", min=1, max=23),
                            dcc.Dropdown(
                            id='time-selector', 
                            options=[
                                {'label': 'Hour', 'value': 'hour'},
                                {'label': 'Day', 'value': 'day'},
                                {'label': 'Week', 'value': 'week'}
                                ],
                                value='hour',
                                searchable=False,
                                clearable=False,
                                style={'width':'50%'}
                            ),
                        ],),
                    ], style={'display': 'inline'}),

                    html.Div(id="daterange-container", children= [
                        dcc.DatePickerRange(
                            id='daterange-picker',
                            clearable=True,
                            show_outside_days=True,
                            min_date_allowed=fileframes.index[0].replace(hour=0, minute=0, second=0, microsecond=0),
                            max_date_allowed=(fileframes.index[-1] + timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0),
                            initial_visible_month=fileframes.index[0],
                            minimum_nights=0,
                            start_date_placeholder_text='Start date',
                            end_date_placeholder_text='End date',
                            ),
                        ], style={'display': 'none'}),

                ],
                width={"size": 6},
                align='center'),
            ],),

            
            html.Br(),
            html.Br(),


            html.Br(),
            html.Br(),
            #html.Br(),
            #html.Br(),
            #html.Br(),
            #html.Br(),
            #html.Br(),
            #html.Br(),
            #html.Br(),
            #html.Br(),
            #html.Br(),
            #html.Br(),
            #html.Br(),
            #html.Br(),
            #html.Br(),

            dbc.Row(children=[
                dbc.Col(children=
                    dbc.Collapse(children=
                        dcc.RangeSlider(
                            id='hour-selector',
                            min=0,
                            max=24,
                            marks={0: '00h00', 3: '03h00', 6: '06h00', 9: '09h00', 12: '12h00', 15: '15h00', 18: '18h00', 21: '21h00', 24: '00h00'},
                            value=[0, 24]),
                        id="hour-selector-collapse",
                        is_open=False),
                    width={"size": 12},
                    align='center'
                    ),
                ],
            ),

            html.Br(),
            html.Br(),

            dbc.Row(children=[
                dbc.Col(children=
                    dbc.Button("Send", id="send-dialog-button"),
                    width={"size": 2, "offset": 5},
                    align='center'
                    ),
                ],
            ),
        ],
    ),
    

    # Test for SDDrawer (not docked)
    sdmu.Drawer(id='drawer', docked=False, disableSwipeToOpen=True, open=False, width='20%', children=[
        dbc.Form([
            dbc.FormGroup(
                [
                dbc.Label("Choose Filter", style={'font-weight': 'bold', 'padding-left': '2%'}),
                dbc.RadioItems(
                    id='filterOptions',
                    options=[
                    {'label': 'No Filter', 'value': 'normal'},
                    {'label': 'Butterworth Filter', 'value': 'butter'}],
                    value='normal', style={'padding-left': '3%'})
                ]
            ),

            dbc.Collapse(children=
                dbc.Card([
                        dbc.CardHeader("Butterworth Filter Options"),
                        dbc.CardBody(
                            [
                                dbc.Form([
                                        dbc.Label("Filter Type :"),
                                        dcc.Dropdown(
                                            id='filter_type', 
                                            options=[
                                                {'label': 'High Pass (HPF)', 'value': 'high'},
                                                {'label': 'Low Pass (LPF)', 'value': 'low'},
                                                {'label': 'Band Pass (BPF)', 'value': 'band'}
                                            ],
                                            value='band',
                                            searchable=False,
                                            clearable=False,
                                            style={'width':'70%', 'padding-left': '1%'}
                                        )
                                ], inline=True),

                                dbc.Form([
                                    dbc.Label("Higher Sample Rate :"),
                                    dbc.Col(
                                        dbc.InputGroup(
                                            [
                                            dbc.InputGroupAddon("1/", addon_type="prepend"),
                                            dcc.Input(id='hpf', min=1, value=10, type="number", style={'width':'40%'}),
                                            ], 
                                        ),
                                    ),
                                ], inline=True),

                                dbc.Form([
                                    dbc.Label("Lower Sample Rate :"),
                                    dbc.Col(
                                        dbc.InputGroup(
                                            [
                                            dbc.InputGroupAddon("1/", addon_type="prepend"),
                                            dcc.Input(id='lpf', min=1, value=100, type="number", style={'width':'40%'}),
                                            ],
                                        ),
                                    ),
                                ], inline=True),

                                dbc.Form([
                                    dbc.Label("Percentile :"),
                                    dbc.Col(
                                        dbc.InputGroup(
                                            [
                                            dcc.Input(id='percentile', value=15, max=50, type="number", style={'width':'30%'}),
                                            dbc.InputGroupAddon("%", addon_type="append"),
                                            ]
                                        ),
                                    ),
                                ], inline=True),
                            ]),
                    ],),
                is_open=True, id="ButterFilterCollapse", style={'padding-left': '2%', 'width':'96%'}
            ),

            
            dbc.Label("Normalization Options:", style={'font-weight': 'bold', 'padding-left': '2%'}),
            dbc.Form(
                [
                dbc.Label("Percentile : "),
                dcc.Input(id='norm_percentile', value=15, type="number", min=0, max=50, style={'padding-left': '1%'}),
                ], inline=True, style={'padding-left': '3%'}
            ),

            html.Br(),
            
            dbc.Label("Select Correlation Indexes", style={'font-weight': 'bold', 'padding-left': '2%'}),
            dbc.Form(
                [
                dbc.Label("Input A : "),
                dcc.Input(id='input_a', value=6, type="number", min=0, max=15, style={'padding-left': '1%'}),
                ], inline=True, style={'padding-left': '3%'}
            ),
            dbc.Form(
                [
                dbc.Label("Input B : "),
                dcc.Input(id='input_b', value=15, type="number", min=0, max=15, style={'padding-left': '1%'}),
                ], inline=True, style={'padding-left': '3%'}
            ),

            html.Br(),

            dbc.Label("Visibility Type", style={'font-weight': 'bold', 'padding-left': '2%'}),

            dcc.Dropdown(
                id='visibility_type', 
                options=[
                    {'label': 'Real', 'value': 'real'},
                    {'label': 'Imaginary', 'value': 'imag'},
                    {'label': 'Magnitude', 'value': 'mag'},
                    {'label': 'Phase', 'value': 'phase'},
                    #{'label': 'Complex', 'value': 'complex'}
                ],
                value='real',
                searchable=False,
                clearable=False,
                style={'width':'50%', 'padding-left': '3%'}
            ),


            html.Br(),

            html.Div(children=[dbc.Button("Apply", id="apply-filter-button")], style={'padding-left': '2%'}),

            html.Br(),
            ],
        )
    ]),

    dbc.Container(children=[
        dbc.Row(children=[
            dbc.Col(children=
                html.H2(children='HIRAX Firstlook', style={'textAlign': 'center', 'color': colors['text']}),
                width={"size": 6, "offset": 3},
                align='center'
                ),
            ],
        ),

        dbc.Row(children=[
            dbc.Col(children=
                dbc.Button(">", id="side-drawer-button"),
                width={"size": 1},
                align='left'
                ),
            dbc.Col(children=
                html.H4(children='1st Prototype Interface for HIRAX Quicklook', style={'textAlign': 'center', 'color': colors['text']}),
                width={"size": 6, "offset": 2},
                align='center'
                ),
            ],
        ),

        dbc.Row(children=[
            dbc.Col(children=
                [
                dcc.Graph(id='heatmap', figure={}, style={'height':'70vh', 'width':'55vw'}), #go.Figure(data=[visData], layout=visLayout)
                #dcc.Graph(id='fornax', figure={}), #go.Figure(data=[visFornaxData], layout=visFornaxLayout)
                ],
                width=7
            ),

            dbc.Col(children=
                [
                dbc.Label("Slice Mode", style={'font-weight': 'bold'}),

                dbc.RadioItems(
                    id='sliceOptions',
                    options=[
                    {'label': 'Frequency ', 'value': 'frequency'},
                    {'label': 'Time ', 'value': 'time'}],
                    value='frequency'),

                html.Br(),

                html.Div(id="frequency-slice-container", children= [
                    dcc.Dropdown(
                        id='frequency-options', 
                        options=frequency_dictionary, 
                        multi=True,
                        placeholder="Specify a frequency",
                        ),

                    dbc.Collapse(children=html.Div(dcc.Graph(id='frequency-fringes-graph', figure={}), style={'overflowY': 'scroll', 'max-height': '50vh'}),
                            id="frequency-fringes-collapse",
                            is_open=False,
                        ),
                    ], style={'display': 'inline'}),

                html.Div(id="time-slice-container", children=[
                    html.Div([dbc.Label(" Date: ")], style={'padding-right': '1%', 'display': 'inline-block'}), 
                    html.Div([
                        dcc.DatePickerSingle(
                            id='date-picker-single',
                            date=fileframes.index[0].replace(hour=0, minute=0, second=0, microsecond=0),
                            min_date_allowed=fileframes.index[0].replace(hour=0, minute=0, second=0, microsecond=0),
                            max_date_allowed=fileframes.index[-1].replace(hour=0, minute=0, second=0, microsecond=0),
                            )]
                        , style={'padding-bottom': '1%', 'padding-right': '3%', 'display': 'inline-block'}),
                    
                    html.Div([dbc.Label(" Hours: ")], style={'padding-right': '1%', 'display': 'inline-block'}), 
                    html.Div([dcc.Input(id='hourBox', value=0, min=0, max=23, type="number")], style={'padding-right': '3%', 'display': 'inline-block'}),
                    html.Div([dbc.Label(" Minutes: ")], style={'padding-right': '1%', 'display': 'inline-block'}), 
                    html.Div([dcc.Input(id='minuteBox', value=0, min=0, max=59, type="number")], style={'padding-right': '3%', 'display': 'inline-block'}),
                    html.Div([dbc.Label(" Seconds: ")], style={'padding-right': '1%', 'display': 'inline-block'}), 
                    html.Div([dcc.Input(id='secondBox', value=0, min=0, max=59, type="number")], style={'padding-right': '3%', 'display': 'inline-block'}),
                    html.Div([dbc.Button(id='add-timestamp-btn', className='fa fa-plus')], style={'display': 'inline-block'}),

                    dcc.Dropdown(
                        id='time-options', 
                        options=timestamp_dictionary, 
                        multi=True,
                        placeholder="Specify a time",
                        ),

                    dbc.Collapse(children=html.Div(dcc.Graph(id='time-fringes-graph', figure={}), style={'overflowY': 'scroll', 'max-height': '50vh'}),
                            id="time-fringes-collapse",
                            is_open=False,
                        ),
                    ], style={'display': 'none'}),
                ],
                width=5
            )
        ],),
    ], fluid=True)
])




@app.callback(
    Output('data-selector-dialog', 'open'), 
    [Input('send-dialog-button', 'n_clicks')])
def dialogInformation(clicks):
    if clicks is None:
        return True
    return False

@app.callback(
    [Output('local-storage', 'data'), Output('time-options', 'options'), Output('apply-filter-button', 'n_clicks'), Output('date-picker-single', 'min_date_allowed'), Output('date-picker-single', 'max_date_allowed')], 
    [Input('send-dialog-button', 'n_clicks')], 
    [State('local-storage', 'data'), State('hour-selector-collapse', 'is_open'), State('calander-checkbox', 'values'), State('time-value', 'value'), State('time-selector', 'value'), State('daterange-picker', 'start_date'), State('daterange-picker', 'end_date'), State('hour-selector', 'value')])
def storeDatetimes(clicks, data, hour_selection, calendar_selection, timeValue, timeMode, startdate, enddate, timerange):
    #global clients
    #global brain
    if clicks is None:
        raise PreventUpdate
    maximumTime, minimumTime = None, None

    # Get client number for global array
    print(redisClient.get('clients'))
    clientNo = int(redisClient.get('clients'))
    redisClient.incr('clients')
    #clients = clients + 1

    if len(calendar_selection)==0:
        maximumTime = fileframes.index[-1]
        if timeMode=='hour':
            minimumTime = maximumTime - timedelta(hours=timeValue)
        if timeMode=='day':
            minimumTime = maximumTime - timedelta(days=timeValue)
        if timeMode=='week':
            minimumTime = maximumTime - timedelta(days=7*timeValue)
    else:
        maximumTime = datetime.strptime(enddate, '%Y-%m-%d')
        minimumTime = datetime.strptime(startdate, '%Y-%m-%d')
        if hour_selection:
            #print('Here')
            maximumTime = maximumTime + timedelta(hours=timerange[1])
            minimumTime = minimumTime + timedelta(hours=timerange[0])

    # Find upper file range
    fileframe_upper_cut = fileframes[fileframes.index < maximumTime]
    # Find lower file range
    fileframe_final_cut = fileframe_upper_cut[fileframe_upper_cut.index > minimumTime]
    # Get the filename column of data within the given range
    file_list = np.unique(fileframe_final_cut['filename'].values)
    file_list = list(np.unique(file_list))


    maximumTime = maximumTime.replace(tzinfo=timezone.utc)
    minimumTime = minimumTime.replace(tzinfo=timezone.utc)

    visuals = gc.initialise(filenames=file_list, input_a=6, input_b=15, vis_type='real')

    if minimumTime > visuals['datetimes'][-1]:
        minimumTime = visuals['datetimes'][-1] - timedelta(hours=1)

    # Update storage to latest timestamp
    #data = [minimumTime + timedelta(hours=2), maximumTime + timedelta(hours=2), clientNo]
    data = [minimumTime, maximumTime, clientNo]    #clientNo
    #print(data)

    # Update dictionary and visibility data indexes
    _, lower_index, upper_index = gc.indexedVisuals(visuals, data[0], data[1])
    _, time_dict = dictionaries(visuals, lower = lower_index, upper = upper_index)
    
    #with open('visibility-%s.pkl' % clientNo, 'wb') as output:
    #    pickle.dump(visuals, output, pickle.HIGHEST_PROTOCOL)

    #print(np.shape(brain['memorybank']))
    redisClient.rpush('visGraph', pickle.dumps(visuals, protocol=0))
    #print(pickle.loads(redisClient.lindex('visGraph', clientNo)))
    #print(time_dict)
    #print(visuals)
    #visGraph[clientNo] = visuals
    #brain['memorybank'][clientNo] = visuals


    #temp = brain['memorybank']
    #print(temp)
    #temp[clientNo] = visuals
    #print(brain['memorybank'][clientNo])
    #brain['memorybank'] = temp

    return data, time_dict, 1, minimumTime.replace(hour=0, minute=0, second=0, microsecond=0), maximumTime.replace(hour=0, minute=0, second=0, microsecond=0)

@app.callback(
    Output('ButterFilterCollapse', 'is_open'), 
    [Input('filterOptions', 'value')])
def pullFilterOptions(value):
    if value=='butter':
        return True
    elif value=='normal':
        return False


@app.callback(
    Output('heatmap', 'figure'), #, Output('fornax', 'figure') 
    [Input('apply-filter-button', 'n_clicks')], 
    [State('local-storage', 'data'), State('visibility_type', 'value'),  State('filter_type', 'value'), State('hpf', 'value'), State('lpf', 'value'), State('percentile', 'value'), State('filterOptions', 'value'), State('heatmap', 'figure'), State('input_a', 'value'), State('input_b', 'value'), State('norm_percentile', 'value')]) #, State('fornax', 'figure')
def updateInformation(apply_clicks, data, visibilityType, filterType, hpf, lpf, filter_percentile, selection, heatmapFigure, inputA, inputB, norm_perc):
    if apply_clicks is None:
        return heatmapFigure #, fornaxFigure

    print(dateparse(data[0]).replace(tzinfo=timezone.utc), dateparse(data[1]).replace(tzinfo=timezone.utc), int(data[2]))

    clientNo = int(data[2])
    print(clientNo)


    visuals = pickle.loads(redisClient.lindex('visGraph', clientNo))
    #visuals = brain['memorybank'][clientNo]
    #visuals = visGraph[clientNo]
    #print(visuals)
    #with open('visibility-%s.pkl' % clientNo, 'rb') as input:
    #    visuals = pickle.load(input)
    #    print(visuals)
    
    if ((visuals['input_a'] != inputA) or (visuals['input_b']!=inputB) or (visuals['visibility_type']!=visibilityType)):
        visuals = gc.initialise(filenames=visuals['files'] , input_a=inputA, input_b=inputB, vis_type=visibilityType)
        
        gc.indexedVisuals(visuals, dateparse(data[0]).replace(tzinfo=timezone.utc), dateparse(data[1]).replace(tzinfo=timezone.utc))

    if selection=='butter':
        if filterType=='band':
            freqs=(1/lpf,1/hpf)
        elif filterType=='low':
            freqs=(1/lpf)
        elif filterType=='high':
            freqs=(1/hpf)
        visuals['plotData'] = gc.butterworth(visuals, percentile=filter_percentile, char_freqs=freqs, band=filterType)
    elif selection=='normal':
        visuals['plotData'] = gc.unfilterData(visuals)

    if visibilityType == 'real':
        colour = 'RdBu'
    elif visibilityType == 'imag':
        colour = 'Viridis'
    elif visibilityType == 'mag':
        colour = 'YlGnBu'
    elif visibilityType == 'phase':
        colour = 'Cividis'
    new_figure = gc.plot(visuals, percentile=norm_perc, cmap=colour)


    #with open('visibility-%s.pkl' % clientNo, 'wb') as output:
    #    pickle.dump(visuals, output, pickle.HIGHEST_PROTOCOL)

    #visGraph[clientNo] = visuals

    redisClient.lset('visGraph', clientNo, pickle.dumps(visuals, protocol=0))

    #temp = brain['memorybank']
    #temp[clientNo] = visuals
    #brain['memorybank'] = temp

    return new_figure


@app.callback(
    Output('frequency-fringes-graph', 'figure'), 
    [Input('frequency-options', 'value'), Input('heatmap', 'figure')], 
    [State('local-storage', 'data'), State('frequency-fringes-graph', 'figure')])
def updateFrequencyFringes(value, heatmapFigure, data, figure):
    if value is None:
        return {}
    elif len(value)==0:
        return {}
    else:
        if heatmapFigure=={}:
            return figure
        else:
            clientNo = int(data[2])
            #visuals = visGraph[clientNo]
            visuals = pickle.loads(redisClient.lindex('visGraph', clientNo))
            #visuals = brain['memorybank'][clientNo]

            #visuals = redisClient.get('visGraph')[clientNo]
            #with open('visibility-%s.pkl' % clientNo, 'rb') as input:
            #    visuals = pickle.load(input)
            #    print(clientNo)
            return gc.plotFrequencyFringes(visuals, test_freqs=value)

@app.callback(
    Output('time-fringes-graph', 'figure'), 
    [Input('time-options', 'value'), Input('heatmap', 'figure')], 
    [State('local-storage', 'data'), State('time-fringes-graph', 'figure')])
def updateTimeFringes(value, heatmapFigure, data, figure):
    if value is None:
        return {}
    elif len(value)==0:
        return {}
    else:
        if heatmapFigure=={}:
            return figure
        else:
            clientNo = int(data[2])
            #visuals = visGraph[clientNo]
            visuals = pickle.loads(redisClient.lindex('visGraph', clientNo))
            #visuals = brain['memorybank'][clientNo]
            
            #visuals = redisClient.get('visGraph')[clientNo]
            #with open('visibility-%s.pkl' % clientNo, 'rb') as input:
            #    visuals = pickle.load(input)
            return gc.plotTimeFringes(visuals, test_times=value)

@app.callback(
    Output('time-options', 'value'), 
    [Input('add-timestamp-btn', 'n_clicks')], 
    [State('time-options', 'value'), State('time-options', 'options'), State('date-picker-single','date'), State('hourBox', 'value'), State('minuteBox', 'value'), State('secondBox', 'value')])
def updateTimeFringes(timeBtn_clicks, value, options, date, hrs, mins, sec):
    if timeBtn_clicks is None:
        return []
    else:
        specified_date = dateparse(date).replace(hour=hrs, minute=mins, second=sec, tzinfo=timezone.utc).timestamp()
        date_value_list = [value['value'] for value in options]
        # Find closest time
        index = np.argmin((np.array(date_value_list) - specified_date)**2)
        value.append(date_value_list[index])
        return value

@app.callback(
    Output('drawer', 'open'), 
    [Input('side-drawer-button', 'n_clicks')], [State('drawer', 'open')])
def openPanel(clicks, state):
    if clicks is None:
        return False
    if state:
        return False
    if not state:
        return True


@app.callback(
    Output('frequency-fringes-collapse', 'is_open'), 
    [Input('frequency-fringes-graph', 'figure')])
def open_close_frequency_slice_graph(inputFigure):
    if inputFigure=={}:
        return False
    else:
        return True

@app.callback(
    Output('time-fringes-collapse', 'is_open'), 
    [Input('time-fringes-graph', 'figure')])
def open_close_time_slice_graph(inputFigure):
    if inputFigure=={}:
        return False
    else:
        return True





@app.callback(
    [Output('frequency-slice-container', 'style'), Output('time-slice-container', 'style')], 
    [Input('sliceOptions', 'value')])
def hide_show_frequency_slice_options(inputOption):
    if inputOption=="frequency":
        return {'display': 'inline'}, {'display': 'none'}
    else:
        return {'display': 'none'}, {'display': 'inline'}


@app.callback(
    [Output('daterange-container', 'style'), Output('time-value-container', 'style')], 
    [Input('calander-checkbox', 'values')])
def hide_show_daterange_options(inputOption):
    if len(inputOption)==1:
        return {'display': 'inline'}, {'display': 'none'}
    else:
        return {'display': 'none'}, {'display': 'inline'}




@app.callback(
    [Output('time-value', 'max'), Output('time-value', 'value')], 
    [Input('time-selector', 'value')], 
    [State('time-value','value')])
def time_maximum_selection_max(inputOption, value):
    if inputOption=='hour':
        maximum = 23
    if inputOption=='day':
        maximum = 6
    if inputOption=='week':
        maximum = 3
    if value > maximum:
        return maximum, maximum
    else:
        return maximum, value


@app.callback(Output('hour-selector-collapse', 'is_open'), [Input('daterange-picker', 'start_date'), Input('daterange-picker', 'end_date'), Input('calander-checkbox', 'values')])
def hide_show_hour_specifier(start, end, checkBox):
    if len(checkBox) == 0:
        return False
    if start==end:
        if start!=None:
            return True
    else:
        return False


if __name__ == '__main__':
    app.run_server(debug=True,use_reloader=False)
