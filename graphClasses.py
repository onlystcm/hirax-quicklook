# Plotly dependencies
import plotly.tools as tls
import plotly.graph_objs as go

# Matplotlib dependencies
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import dates

# SciPy Dependencies
from scipy import signal

# HDF5 dependencies for binary data format
import h5py

# Numpy dependencies
import numpy as np

# Time libraries
from datetime import datetime, timedelta, timezone

# Import moduule for HiRAX Tools
from hirax_tools.core import RawData

# Astropy. A Community Python Library for Astronomy
from astropy.visualization import PercentileInterval, ImageNormalize, LinearStretch
from astropy import units
from astropy import coordinates as coords

# Python debugger
import pdb

# Pre-defined co-ordinates
hartrao = coords.EarthLocation.from_geodetic(lon=27.6853931*units.deg, lat=-25.8897515*units.deg, height=1415.710*units.m, ellipsoid='WGS84')
alt, az = (90-10.2)*units.deg, 180*units.deg
fornax_A = coords.SkyCoord(ra='03h22m41.7s', dec='-37d12m30s')

#class visibility():
def initialise(filenames, input_a, input_b, vis_type):
	#pdb.set_trace()
	this = {}
	this['files'] = filenames
	this['visuals'], this['real_visuals'], this['complex_visuals'], this['imag_visuals'] = [], [], [], []
	this['times'] = []
	datetimes = []

	# Set correlator input
	this['input_a']=input_a
	this['input_b']=input_b
	this['visibility_type'] = vis_type
	#print(vis_type)
	this['lower_index']=None
	this['upper_index']=None

	for filename in filenames:
	    data = RawData(filename)
	    # Get frequency bands
	    this['freq'] = data.bands
	    # Get visibilities
	    if len(this['times']) == 0:
	        this['visuals'] = data.visibilities((this['input_a'], this['input_b']), which=this['visibility_type'])
	        #this['real_visuals = data.visibilities((this['input_a, this['input_b), which='real')
	        #this['imag_visuals = data.visibilities((this['input_a, this['input_b), which='imag')
	        #this['complex_visuals = data.visibilities((this['input_a, this['input_b), which='complex')
	        this['times'] = data.times
	        datetimes = data.times.datetime
	    else:
	        this['visuals'] = np.append(this['visuals'], data.visibilities((this['input_a'], this['input_b']), which=this['visibility_type']), axis=0)
	        #this['real_visuals = np.append(this['real_visuals, data.visibilities((this['input_a, this['input_b), which='real'), axis=0)
	        #this['imag_visuals = np.append(this['imag_visuals, data.visibilities((this['input_a, this['input_b), which='imag'), axis=0)
	        #this['complex_visuals = np.append(this['complex_visuals, data.visibilities((this['input_a, this['input_b), which='complex'), axis=0)
	        this['times'] = np.append(this['times'], data.times, axis=0)
	        datetimes = np.concatenate((datetimes, data.times.datetime))

	# Use unfiltered data
	this['plotData'] = []

	# Get recording times
	this['times_as_numbers'] = np.array([float(str(time)) for time in this['times']])
	this['datetimes'] = np.array([time.replace(tzinfo=timezone.utc) for time in datetimes])
	#print(this['datetimes'])

	# Determine where the telescope is pointing in J2000 coordinates
	altazs = coords.SkyCoord(frame='altaz', alt=alt*np.ones(len(this['times'])),az=az*np.ones(len(this['times'])), location=hartrao, obstime=this['times'])
	j2000_pointing = altazs.transform_to(coords.ICRS)

	# Find on-sky pointing separation of these coordinates with respect to Fornax A
	this['pointing_separation'] = j2000_pointing.separation(fornax_A)
	this['times'] = np.array([str(time) for time in this['times']])
	this['visuals'] = this['visuals']
	return this

def indexedVisuals(this, lower_datetime=None, upper_datetime=None):
	if (lower_datetime is None) and (upper_datetime is None):
		# Select data as per time indexes and type
		visuals = this['visuals'][this['lower_index']:this['upper_index'],:]
		return visuals, None, None
	else:
		# Get indexes of data within datetime
		upper_datetime_index = np.where(this['datetimes'] < upper_datetime)[0][-1]
		lower_datetime_index = np.where(this['datetimes'] > lower_datetime)[0][0]

		# Select data as per time indexes and type
		visuals = this['visuals'][lower_datetime_index:upper_datetime_index,:]

		this['lower_index'] = lower_datetime_index
		this['upper_index'] = upper_datetime_index
		return visuals, lower_datetime_index, upper_datetime_index


def unfilterData(this):
	# Plot visibilities
	visuals, _ , _ = indexedVisuals(this)
	norm_visuals = visuals - np.median(visuals)
	return norm_visuals


def plot(this, lower_index=None, upper_index=None, percentile=None, cmap='RdBu'):
	plotdata = this['plotData']
	# Saturate outside input percentile
	if percentile is not None and (percentile > 0):
		'''The following statements implement the ImageNormalize function in the astropy.visualization library'''
		interval = PercentileInterval(percentile)
		minimum, maximum = interval.get_limits(plotdata)
		#print('Min and Max is ', minimum, maximum)
		# Make sure scalars get broadcast to 1-d
		if np.isscalar(plotdata):
			plotdata = np.array([plotdata], dtype=plotdata.dtype)
		else:
			# copy because of in-place operations after
			plotdata = np.array(plotdata, copy=True, dtype=plotdata.dtype)
		#plotdata[np.where((plotdata < minimum).any() or (plotdata > maximum).any())] = float('Inf')
		percs = np.percentile(plotdata, q=(min(percentile, 100-percentile), max(percentile, 100-percentile)))
		minimum, maximum = min(percs), max(percs)
		plotdata[plotdata < minimum] = minimum #np.inf
		plotdata[plotdata > maximum] = maximum #np.inf
		
		# Normalize based on vmin and vmax
		plotdata = np.subtract(plotdata, minimum)
		plotdata = np.true_divide(plotdata, maximum - minimum)
		#stretch = LinearStretch()
		#plotdata = stretch(plotdata, clip=False)
		#plotdata = np.clip(plotdata, 0., 1., out=plotdata)
		'''End of statements'''

	# plotly figure data
	trace_heatmap = go.Heatmap(z=plotdata, x=this['freq'], y=this['datetimes'][this['lower_index']:this['upper_index']], colorscale=cmap,)
	

	trace_fornax = go.Scattergl(
		y=this['datetimes'][this['lower_index']:this['upper_index']],
		x=this['pointing_separation'][this['lower_index']:this['upper_index']],
		mode='lines',
		name='Fornax A',
		)

	'''
	layout_fornax = go.Layout(
		xaxis=dict(title='UTC'),
		yaxis=dict(title='Separation \n [deg]',),
		scene=dict(aspectmode='auto'),
		)

	# plotly figure layout
	layout_heatmap = go.Layout(
		#xaxis=dict(title='Frequency [MHz]',range=[this['freq'][0], this['freq'][-1]]),
		#yaxis=dict(title='UTC', autorange='reversed', automargin=True),
		margin=go.layout.Margin(l=100, r=100, b=100, t=100, pad=4),
		scene=dict(aspectmode='auto'),
		title='Visibility',
		)
	'''

	fig = tls.make_subplots(rows=1, cols=2, shared_yaxes=True)
	fig.append_trace(trace_heatmap, 1, 1)
	fig.append_trace(trace_fornax, 1, 2)
	fig['layout'].update(
		xaxis1=dict(title='Frequency [MHz',range=[this['freq'][0], this['freq'][-1]], domain=[0, 0.8]),
		xaxis2=dict(title='Separation\n[deg]', domain=[0.8, 1]),
		yaxis1=dict(title='UTC', autorange='reversed', automargin=True),
		yaxis2=dict(autorange='reversed', automargin=True,),
		#height='80vh',
		#width='30vh',
		#scene=dict(aspectmode='manual', aspectratio=dict(x=1, y=1, z=1)),
		title='Correlation Visibility')
	return fig#trace, layout

def butterworth_filter(data, filter_type, char_freqs, order=4, sample_rate=1, axis=None):
	if axis is None:
		axis_size = len(data)
		axis = -1
	else:
		axis_size = data.shape[axis]
		# FT data on specified axis
		vis_fft = np.fft.fft(data, axis=axis)
		# scipy signal processing implementation details...
		b, a = signal.butter(N=order, Wn=char_freqs, btype=filter_type, analog=True)
		freqs = np.fft.fftfreq(axis_size, d=sample_rate)
		_, h = signal.freqs(b, a, freqs)
		# This ensures things broadcast correctly 
		# depending on the specified axis.
		broadcast_axes = [None for i in range(data.ndim)]
		broadcast_axes[axis] = slice(None, None, -1)
		# filter data on axis specified
		h = np.abs(h)
		filtered_vis_fft = vis_fft*(h[tuple(broadcast_axes)]**2)
		return np.fft.ifft(filtered_vis_fft, axis=axis)
		'''!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''

def butterworth(this, percentile=15, char_freqs=(1/100,1/10), band='band'):
	visuals, _ , _ = indexedVisuals(this)
	bandpass = butterworth_filter(visuals, char_freqs=char_freqs, filter_type=band, axis=0)
	if this['visibility_type'] == 'real':
		bandpass = bandpass.real
	elif this['visibility_type'] == 'imag':
		bandpass = bandpass.imag
	elif this['visibility_type'] == 'mag':
		bandpass = np.absolute(bandpass)
	elif this['visibility_type'] == 'phase':
		bandpass = np.angle(bandpass)
	percs = np.percentile(visuals, q=(percentile, 100-percentile))
	bandpass[visuals < percs[0]] = np.inf
	bandpass[visuals > percs[1]] = np.inf
	return bandpass

def plotFornax(this): 
	trace = go.Scattergl(
		y=this['datetimes'][this['lower_index']:this['upper_index']],
		x=this['pointing_separation'][this['lower_index']:this['upper_index']],
		mode='lines',
		name='Fornax A',)
	layout = go.Layout(
		#axis=dict(title='UTC'),
		#axis=dict(title='Separation \n [deg]',),
		scene=dict(aspectmode='auto'),
		)
	return trace, layout

def plotFrequencyFringes(this, test_freqs, visuals=None):
	vis = this['plotData']
	# Convert list of values to frequency in a presentable format
	selectedFrequencies = [str('{:.1f} GHz'.format(frequency)) for frequency in test_freqs]
	
	fig = tls.make_subplots(rows=len(test_freqs), cols=1, subplot_titles=selectedFrequencies)

	fringes = []

	for i, tfreq in enumerate(test_freqs):
		# Find closest frequency in the dataset
		index = np.argmin((this['freq'] - tfreq)**2)
		band = this['freq'][index]
		fringes.append(vis[:, index])
		trace = go.Scattergl(x=this['datetimes'][this['lower_index']:this['upper_index']], y=fringes[i], mode='lines', name=str('{:.1f} GHz'.format(band)))
		position = i+1
		fig.append_trace(trace, position, 1)

	fig['layout'].update(height=(450*(len(test_freqs))), title='Frequency Slices')
	return fig

def plotTimeFringes(this, test_times, visuals=None):
	vis = this['plotData']
	#print(this['plotData'])
	selectedTimestamps = [(datetime.fromtimestamp(timestamp, tz=timezone.utc)).strftime("%d/%m/%Y, %H:%M:%S") for timestamp in test_times]

	# Convert list of values to frequency in a presentable format
	fig = tls.make_subplots(rows=len(test_times), cols=1, subplot_titles=selectedTimestamps)

	fringes = []

	for i, timestamp in enumerate(test_times):
		# Find closest time
		index = np.argmin((this['times_as_numbers'][this['lower_index']:this['upper_index']] - timestamp)**2)
		slot = datetime.fromtimestamp(float(this['times'][index]))
		fringes.append(vis[index, :])
		trace = go.Scattergl(x=this['freq'], y=fringes[i], mode='lines', name=slot.strftime("%d/%m/%Y, %H:%M:%S"))
		position = i+1
		fig.append_trace(trace, position, 1)

	fig['layout'].update(height=(450*(len(test_times))), title='Time Slices')
	return fig
